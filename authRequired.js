const jwt = require('jsonwebtoken')
const Router = require('koa-router')

const  { jwtSecret } = require('./config')


async function authRequired(ctx) {
  const { authorization } = ctx.request.headers
  const [_, token] = authorization.split(' ')
  
  try {
    const decoded = jwt.verify(token, jwtSecret);
    ctx.body = decoded
  } catch {
    ctx.throw(403)
  }
}

const router = new Router()

router.get('/auth', authRequired)

module.exports = router