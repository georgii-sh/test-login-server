module.exports = {
  jwtSecret: process.env.JWT_SECRET || 'test_secret',
  correctEmail: process.env.EMAIL || 'correct@email.com',
  correctPassword: process.env.PASSWORD || 'pass',
}