const jwt = require('jsonwebtoken')
const Router = require('koa-router')

const  { jwtSecret, correctEmail, correctPassword } = require('./config')

async function validate(ctx, next) {
  ctx.checkBody('email').isEmail('Wrong email')
  ctx.checkBody('password').notEmpty().len(3, 100)

  if (ctx.errors) {
    ctx.body = ctx.errors
    return
  }

  await next()
}

async function login(ctx, next) {
  const { body: { email, password } } = ctx.request

  if (email !== correctEmail || password !== correctPassword) {
    return ctx.throw(401)
  }
 
  const token = jwt.sign({ id: 1, name: 'John Doe' }, jwtSecret)

  ctx.body = { token }
}

const router = new Router()

router.post('/login', validate, login)

module.exports = router