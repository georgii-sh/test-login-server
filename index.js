const Koa = require('koa')
const Router = require('koa-router')
const koaBody = require('koa-body')
const loginRouter = require('./login')
const authRequiredRouter = require('./authRequired')

const app = new Koa()

function index(ctx, next) {
  ctx.body = 'API index'
}

app.use(koaBody())

const router = new Router()
router.get('/', index)

app
  .use(router.routes())
  .use(router.allowedMethods())
  .use(loginRouter.routes())
  .use(loginRouter.allowedMethods())
  .use(authRequiredRouter.routes())
  .use(authRequiredRouter.allowedMethods())

require('koa-validate')(app);

const PORT = process.env.PORT || 5555

app.listen(PORT)
console.log(`Koa listening on port ${PORT}`)