# Test login server

## build and run

Use `npm run build-image` and `docker-compose up`


## Usade

There is 2 routes:

`POST /login` and `GET /auth`

Route `GET /auth` is protected by Bearer token

Route `POST /login` accept email: 'correct@email.com' and password: 'pass'